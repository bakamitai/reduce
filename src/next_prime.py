def is_prime(n):
    if n <= 2:
        return True
    if n % 2 == 0:
        return False

    f = 3
    while f * f <= n:
        if n % f == 0:
            return False
        f += 2

    return True

def next_prime(n):
    if n % 2 == 0:
        n += 1

    while True:
        if is_prime(n):
            return n
        n += 2
