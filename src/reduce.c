#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct
{
    char *str;
    size_t len;
} Entry;

typedef struct
{
    Entry *data;
    size_t len;
    size_t capacity;
} HashSet;

int insert(HashSet *, Entry);

int equals(Entry entry_1, Entry entry_2)
{
    if(entry_1.len != entry_2.len) return 0;

    for(int i = 0; i < entry_1.len; ++i)
        if(entry_1.str[i] != entry_2.str[i]) return 0;

    return 1;
}

uint64_t hash(char *str, size_t len)
{
    uint64_t p = 97;
    uint64_t m = 1e9 + 9;
    uint64_t mul = 1;
    uint64_t hash_value = 0;

    for(int i = 0; i < len; ++i)
    {
        hash_value += str[i] * mul;
        mul *= p;
    }
    return hash_value % m;
}

void maybe_resize(HashSet *set)
{
    double len = (double) set->len;
    double capacity = (double) set->capacity;

    if((len / capacity) >= 0.7)
    {
        Entry *old_data = set->data;
        size_t old_capacity = set->capacity;
        set->data = (Entry *) calloc(set->capacity * 2, sizeof(Entry));
        set->capacity *= 2;
        set->len = 0;
        
        for(int i = 0; i < old_capacity; ++i)
        {
            if(old_data[i].str)
            {
                insert(set, old_data[i]);
            }
        }
        free(old_data);
    }
}

int insert(HashSet *set, Entry e)
{
    size_t idx = hash(e.str, e.len) % set->capacity;

    if(!set->data[idx].str)
    {
        set->data[idx] = e;
        set->len++;
        maybe_resize(set);
        return 1;
    }
    else if(!equals(e, set->data[idx]))
    {
        int probe = 1;
        for(;;)
        {
            idx = (idx + probe * probe) % set->capacity;
            if(!set->data[idx].str)
            {
                set->data[idx] = e;
                set->len++;
                maybe_resize(set);
                return 1;
            }
            else if(equals(e, set->data[idx])) return 0;
            probe++;
        }
    }
}

#define DEFAULT_STR_CAPACITY 1024
#define DEFAULT_HASHSET_CAPACITY 131
int main()
{
    size_t input_len = 0;
    size_t input_capacity = DEFAULT_STR_CAPACITY;
    char *input = (char *) calloc(DEFAULT_STR_CAPACITY, sizeof(char));
    int c;

    while((c = fgetc(stdin)) != EOF)
    {
        if(input_len >= input_capacity - 1)
        {
            input = (char *) realloc(input, input_capacity * 2);
            input_capacity *= 2;
        }
        input[input_len++] = c;
    }

    HashSet filter;
    filter.data = (Entry *) calloc(DEFAULT_HASHSET_CAPACITY, sizeof(Entry));
    filter.capacity = DEFAULT_HASHSET_CAPACITY;
    filter.len = 0;

    char *start = input;
    size_t cursor = 0;
    size_t total = 0;

    while(total < input_len)
    {
        for(; start[cursor] != '\n' && start[cursor] != '\0'; cursor++);
        Entry e = { .str = start, .len = cursor };
        insert(&filter, e);
        
        cursor++; // Move to first character after '\n'
        total += cursor;
        start += cursor;
        cursor = 0;
    }

    for(int i = 0; i < filter.capacity; ++i)
    {
        Entry current = filter.data[i];
        if(current.str)
        {
            for(int j = 0; j < current.len; ++j)
            {
                putc(current.str[j], stdout);
            }
            putc('\n', stdout);
        }
    }

    /* printf("input capacity = %d\n", input_capacity); */

    /* These are only here to make sure I have no memory leaks. */
    /* free(filter.data); */
    /* free(input); */

    return 0;
}
